# Parralix

I built this framework to give me the ability to quickly create parallax sites.

## Installation

`npm install parralix --save`

Once you've installed parralix then you can import it into your project like so.

`import Parralix from 'parralix'`

To initialise the framework run

`Parralix.init()`

You then need to add `data-parralix` to each element that you want the framework to work with

eg `<h1 data-parralix></h1>`

This by itself will add a class to show you where about's it is in the viewport

- aboveView 
- inView 
- belowView

Along with the data-parralix attribute you can also add a `data-y-speed="2"` attribute. This makes the element scroll at a different speed to the speed you are scrolling at. A speed of 1 will make it scroll at the same speed. A scroll speed of 2 will make it scroll twice the speed of the scroll etc.

eg `<h1 data-parralix data-y-speed="2"></h1>`

Similar to data-y-speed you can also add a `data-x-speed` This will make the element move horizontally as you scroll.

You can also add `data-mouse="0.5"` attribute. This makes the element float away in the opposite direction of the mouse and can also give quite a nice parallax effect. You can adjust the amount by changing the value. 0 will not effect the element at all.


## Options

```
Parralix.init({
    mob: 800
});
```

Parralix gives the option to turn the parallax effect off once it reaches a certain viewport size. This can be handy as older mobile devices don't always work well with effects such as this. The above example shows that if the plugin is initialised and the screen size is 800 pixels or less then the parallax effect will be switched off. By default the plugin shows parallax for all screen sizes.

## Using With Vue CLI

Import the module in at the top of your .vue file

```
<script>
import Parralix from 'parralix'

export default {
  name: 'example',
  mounted: function(){
    Parralix.init();
  },
  beforeDestroy: function(){
      Parralix.destroy();
  }  
}
</script>
```

