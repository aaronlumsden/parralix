export default {
  windowWidth: 0,
  windowHeight: 0,
  latestKnownScrollY: 0,
  ticking: false,
  listener: false,
  mouseListener: false,
  mousePos: {x:0,y:0},
  mouseTicking: false,
  items: [],
  options: {
    mob: false,
    offset: 200
  },
  init: function(userOptions){
    
      this.mergeOptions(userOptions);
      this.collectElements();
      
      this.windowWidth = window.innerWidth;
      this.windowHeight = window.innerHeight;
      
      if (window.innerWidth > this.options.mob) {
        this.listener = this.runOnScroll.bind(this);
        window.addEventListener("scroll", this.listener);
        this.mouseListener = this.trackMouse.bind(this);
        window.addEventListener("mousemove", this.mouseListener);
      }
      
      
      
      
      
  },
  mergeOptions: function(userOptions){
    
    if (userOptions && typeof userOptions === "object") {
    
      var option;
      for (option in userOptions) {
        if (userOptions.hasOwnProperty(option)) {
          this.options[option] = userOptions[option]
        }
      }
      
    }
  },
  trackMouse: function(e){
    
    
    this.mousePos.y = this.percentage(this.windowHeight / 2 - e.clientY,this.windowHeight / 2);
    this.mousePos.x = this.percentage(this.windowWidth / 2 - e.clientX,this.windowWidth / 2);
    
    var that = this;
    this.items.forEach(function(item){
      if (item['data']['mouse']) {
        item.element.style.transition = 'transform 1s ease-out';
        item.element.style.transform = 'translateX('+item['data']['mouse']*that.mousePos.x+'px) translateY('+item['data']['mouse']*that.mousePos.y+'px)'
      }
    })
    

  },
  runOnScroll: function(){
    this.latestKnownScrollY = window.scrollY;
    this.requestTick();
  },
  requestTick: function(){
    if(!this.ticking) { requestAnimationFrame(this.update.bind(this)); }
    this.ticking = true;
  },
  update: function(){
    this.ticking = false;
      var that = this;
      this.items.forEach(function($obj){
        
        var $status =  that.getIsInViewData($obj);
        
        
        if (!$obj['element'].classList.contains($status['status'])) {
          $obj['element'].classList.remove('aboveView','belowView','inView');
            $obj['element'].classList.add($status['status']);
        }
        
        if ($status['status'] == 'inView') {
          if ($obj['data']['ySpeed']) {
            $obj['element'].style.transform =  'translateY(-'+$obj['data']['ySpeed']*$status['position']+'px)';
          } else if($obj['data']['xSpeed']){
              $obj['element'].style.transform =  'translateX('+$obj['data']['xSpeed']*$status['position']+'px)';
          }
        }
        
        
      });
      
  },
  percentage: function(a,b){
      return Math.round(a / b * 100);
  },
  getIsInViewData: function($obj){
    
    var $element = $obj['element'].getBoundingClientRect();
    var elementHeight = $obj['element'].offsetHeight;
    var $status;
    var $position;
    var $decimal;


    if (($element.top + elementHeight) <= 0) {
      $status = 'aboveView';
      $position = 100;
      $decimal = 1;
    } else if ( ($element.bottom - elementHeight) <= (window.innerHeight || document.documentElement.clientHeight)){
      $status = 'inView';
      $position = this.percentage(window.innerHeight+elementHeight - $element.bottom, window.innerHeight+elementHeight);
      $decimal = $position/100;
      
    } else if (  ($element.bottom - elementHeight) >= (window.innerHeight || document.documentElement.clientHeight)  ){
      $status = 'belowView';
      $position = 0;
      $decimal = 0;
    }
    

    
  
    
    return {
      status : $status,
      position: $position,
      decimal : $decimal
    }

    
  },
  collectElements: function(){
        var $elements = document.querySelectorAll('[data-parralix]');
        
          
          var that = this;
          $elements.forEach(function($obj){
            
          
            that.items.push({
              'element' : $obj,
              'data' : JSON.parse(JSON.stringify($obj.dataset))
            });
          
          });
        
  },
  destroy: function(){
      window.removeEventListener("scroll", this.listener);
      window.removeEventListener("mousemove", this.mouseListener);
  }
  
}